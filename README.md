---
practica-ISI
---

### 1 - Ir completando 'pizarra': Issues > Boards
### 2 - requirements.txt
    Anotaciones
### 3 - BBDD
    bbdd-isi.dia
### 4 - Ramas
    [tu rama] > donde hacer pruebas
    [master] > lo que se considera definitivo
        
    * Cuando esté lo correcto: 

        - En 'tu rama':
            git status
            git add [lo que se quiere añadir]
            git status > nos aseguramos que se va a enviar lo que queremos 
            git commit -m "[comentario]"
            git push

        ---> git checkout master

        - En 'master': 
            git merge devel 
            git push
                   
### 5 - prompt 'sexy'
   Para ayudarnos a sincronizarnos mejor y ver mejor en qué 'rama' estás y 'qué está pasando':
    
   [SEXY-PROMPT](https://github.com/twolfson/sexy-bash-prompt)

   Seguir los pasos de 'Manual Install', mejor que las anteriores.

   ![](images/sexy-prompt.png)

### 6 - comandos útiles 'git': deshacer cambios

    ---> No hemos hecho 'push':    

        WORKING DIRECTORY
        
            - git restore [FILES]

        STAGE

            - git restore --staged [FILES]

        HISTORY

            - git reset --hard HEAD~1: no mantenemos cambios
            - git reset --soft HEAD~1: mantenemos cambios
