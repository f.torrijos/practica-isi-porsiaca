//
// ---> 'assert': 'must be'
//

import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class BisiestosTest
{
   @Before
   public void setUp ()
   {}

   @After
   public void tearDown ()
   {}

   @Test
   public void testA1B1C1 ()
   {
       assertTrue("multiplo de 4:", Bisiestos.esBisiesto(16));
       assertTrue("multiplo de 100 y 400:", Bisiestos.esBisiesto(800));
       assertFalse("invalido:", Bisiestos.esBisiesto(18));
   }
   
   @Test
   public void testA2B1C1 ()
   {
       assertFalse("multiplo de 4:", Bisiestos.esBisiesto(191));
       assertTrue("multiplo de 100 y 400:", Bisiestos.esBisiesto(1600));
       assertFalse("valido:", Bisiestos.esBisiesto(19));
   }

   @Test
   public void testA1B2C1 ()
   {
       assertTrue("multiplo de 4:", Bisiestos.esBisiesto(196));
       assertFalse("multiplo de 100 y 400:", Bisiestos.esBisiesto(1715));
       assertFalse("valido:", Bisiestos.esBisiesto(0));
   }

   @Test
   public void testA1B1C2 ()
   {
       assertTrue("multiplo de 4:", Bisiestos.esBisiesto(936));
       assertTrue("multiplo de 100 y 400:", Bisiestos.esBisiesto(2400));
       assertTrue("valido:", Bisiestos.esBisiesto(3600));
   }
}
