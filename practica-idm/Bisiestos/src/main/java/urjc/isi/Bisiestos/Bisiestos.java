package urjc.isi.Bisiestos;
import java.lang.IllegalArgumentException;

public class Bisiestos {
   // @param a un numero entero positivo
   // @return true si a es un año bisiesto
   // false en caso contrario.
   // @throws IllegalArgumentException si a no es un parametro valido.
   public static boolean esBisiesto(int a) throws IllegalArgumentException {
      if (a%4 == 0 && (a%100 != 0 || a%400 == 0) && a>=0)
      {
         System.out.println("Año bisiesto");
         return true;
      }
      else
      {
         System.out.println("Año NO bisiesto");
         return false;
      }
   }
}
