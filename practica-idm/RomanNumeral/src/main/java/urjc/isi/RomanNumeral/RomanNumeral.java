//package urjc.isi.RomanNumeral;
import java.lang.IllegalArgumentException;
import java.util.Scanner;
import java.util.*;

public class RomanNumeral{



public static void Checkletters(String s, Map<Character, Integer> map){
	for (int i = 0; i < s.length(); i++){
		if(map.containsKey(s.charAt(i))){
			continue;
		}
		else {
			throw new IllegalArgumentException("roman numeral no valid"); 
		}  
}
}

public static int convierte(String s) {
	//check if valid letters

	Map<Character, Integer> map = new HashMap<Character,Integer>();
	map.put('I',1);
	map.put('V',5);
	map.put('X',10);
	map.put('L',50);
	map.put('C',100);
	map.put('D',500);
	map.put('M',1000);
	
	Checkletters(s,map); 
	
	int result = 0;
	int count = 0;
	Character rom ;
	int k;
	for ( int i = 0; i<s.length()-1;i++){
		rom = s.charAt(i);
		count = 0;
		//los numeros que tienen base 10 o el 1 se pueden repetir hasta 3 veces
		for (int j=i; j <s.length(); j++){
			if ((s.charAt(j)== rom)){
				//cuento los repetidos
				count++;
			}
			if (s.charAt(j) != rom){
				break;
			}
			//controla que no se pueda añadir más de dos veces un numero
			if (count > 3){
				throw new IllegalArgumentException("roman numeral no valid");		
			}
		}
		//no se puede usar dos numeros para restar
		//los numeros que tienen base 5 no se pueden repetir
		if ((s.charAt(i) == 'V')&& (s.charAt(i+1) == 'V')) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		//
		if ((s.charAt(i) == 'L')&& (s.charAt(i+1) == 'L')) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		if ((s.charAt(i) == 'D')&& (s.charAt(i+1) == 'D')) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		//los numeros con base 5 no se pueden usar para restar
		if ((s.charAt(i) == 'V')&& (map.get(s.charAt(i)) < map.get(s.charAt(i+1)))) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		if ((s.charAt(i) == 'L')&& (map.get(s.charAt(i)) < map.get(s.charAt(i+1)))) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		if ((s.charAt(i) == 'D')&& (map.get(s.charAt(i)) < map.get(s.charAt(i+1)))) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		} 
		
		//I solo puede restar a V y X
		
		if ((((s.charAt(i) == 'I') && (map.get(s.charAt(i)) < map.get(s.charAt(i+1))) )&& 
		(map.get(s.charAt(i+1))!= 5)) &&((s.charAt(i) == 'I')&& (map.get(s.charAt(i+1))!= 10))&&((map.get(s.charAt(i)) == map.get(s.charAt(i-1))))) 
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		//X solo puede restar a L y C
		if ((((s.charAt(i) == 'X') && (map.get(s.charAt(i)) < map.get(s.charAt(i+1))) )&& 
		(map.get(s.charAt(i+1))!= 50)) &&((s.charAt(i) == 'X')&& (map.get(s.charAt(i+1))!= 100))&&((map.get(s.charAt(i)) == map.get(s.charAt(i-1)))))
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		if ((((s.charAt(i) == 'C') && (map.get(s.charAt(i)) < map.get(s.charAt(i+1))) )&&
		(map.get(s.charAt(i+1))!= 500)) &&((s.charAt(i) == 'C')&& (map.get(s.charAt(i+1))!= 1000))&&((map.get(s.charAt(i)) == map.get(s.charAt(i-1)))))
		{
			throw new IllegalArgumentException("roman numeral no valid");
		}
		
				
	}
	for (int i = 0 ; i <s.length(); i++){
		if (i> 0 && map.get(s.charAt(i)) > map.get(s.charAt(i-1))){
			result += map.get(s.charAt(i)) - 2*map.get(s.charAt(i-1));
		}
		else {	
			result +=map.get(s.charAt(i));
			
		}
	}
	return result;
}


public static void main (String argv[]){
	  String in;


      // write 'stdin'
      Scanner keyboard = new Scanner(System.in);
      System.out.print ("introduce element: ");
      in = keyboard.nextLine();
      
	  System.out.println ("your number is:" + convierte(in.toUpperCase()));
}
}
