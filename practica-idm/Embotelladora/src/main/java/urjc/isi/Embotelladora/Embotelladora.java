class NoSolution extends Exception {
   String msg;

   NoSolution(String str) {
      msg = str;
   }
   
   public String toString(){
	   return ("NoSolution: " + msg) ;
	}
}

   public class Embotelladora {
      // @param pequenas: numero de botellas en almacen de 1L
      // grandes : numero de botellas en almacen de 5L
      // total : numero total de litros que hay que embotellar
      // @return numero de botellas pequenas necesarias para embotellar el total de liquido, teniendo
      // en cuenta que hay que minimizar el numero de botellas pequenas: primero
      // se rellenan las grandes
      // @throws NoSolution si no es posible embotellar todo el liquido

  public static int calculaBotellasPequenas(int pequenas, int grandes, int total) throws NoSolution{

	//Si nos introducen un parámetro no válido lanzamos la excepcion IllegalArgumentException
	if (pequenas < 0 || grandes < 0 || total < 0){
		throw new IllegalArgumentException("Argumentos ilegales");
	}
      
	//Caso 1: total del litros que debemos embotellas es igual a 0
	if (total == 0){
		return 0;
	}	
      
	//Caso 2: pequenas=0, grandes=0 y total > 0 -> Lanzamos excepcion NoSolution (no podemos embotellar el liquido)
	if  ((total > 0) && (pequenas == 0 && grandes == 0)){
		throw new NoSolution("No es posible embotellar todo el liquido");
	}
      
	//Caso 3: pequenas=0, grandes > 0 y total > 0
	if (pequenas == 0){
		if (total/5 <= grandes){
			return 0;
		}else{
			throw new NoSolution("No es posible embotellar todo el liquido");
		}
	}
      
	//Caso 4: grandes=0, pequenas > 0 y total > 0
	if (grandes == 0){
		if (pequenas >= total){
			return total;
		}else{
			throw new NoSolution("No es posible embotellar todo el liquido");
		}
	}
      
	//Caso 5: pequenas > grandes || grandes > pequenas
	int resto = total - (5*grandes);
		
	if (resto <= 0){ 
		return 0;
	}else{ 
		if (pequenas >= resto){
			return resto;
		}else{
			throw new NoSolution("No es posible embotellar todo el liquido");
		}
	}
}

 public static void main (String args[]) throws NoSolution{
    Embotelladora test = new Embotelladora();
	
	System.out.println(test.calculaBotellasPequenas(10,-10,50));

}
}
