package urjc.isi.DescuentoBlackFriday;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class DescuentoBlackFridayTest
{
   @Before
   public void setUp ()
   {}

   @After
   public void tearDown ()
   {}

   @Test
   public void testA1B2 ()
   {
       System.out.println ("=== testA1B2");
       double precio = 500.0;
       double descuento = 1.5;
       String fecha = "29/11/2021";
       assertFalse(DescuentoBlackFriday.precioFinal(
                      precio, descuento, fecha) < precio); // ko
   }

   @Test
   public void testA2B1 ()
   {
       System.out.println ("=== testA2B1");
       double precio = 500.0;
       double descuento = 0.85;
       String fecha = "30/11/2021";
       assertFalse(DescuentoBlackFriday.precioFinal(
                      precio, descuento, fecha) < precio); // ko
   }

   @Test
   public void testA1B1 ()
   {
       System.out.println ("=== testA1B1");
       double precio = 500.0;
       double descuento = 0.45;
       String fecha = "29/11/2021";
       assertTrue(DescuentoBlackFriday.precioFinal(
                      precio, descuento, fecha) < precio); // ok
   }
}

